package repository.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JdbcUtil {

    private Properties properties;
    private static Connection connection = null;

    public JdbcUtil(Properties properties){
        this.properties = properties;
    }

    private Connection getNewConnection() {

        String driverClassName = properties.getProperty("jdbc.driverClassName");
        String url = properties.getProperty("jdbc.url");
        String username = properties.getProperty("jdbc.username");
        String password = properties.getProperty("jdbc.password");

        Connection newConnection = null;
        try {
            Class.forName(driverClassName);
            newConnection = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return newConnection;
    }


    public Connection getConnection() {
        try {
            if (connection == null || connection.isClosed())
                connection = getNewConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
