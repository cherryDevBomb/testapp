package service.impl;

import model.User;
import repository.UserRepository;
import service.IUserService;

public class UserService implements IUserService {

    private UserRepository userRepo;

    public UserService(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    public User findUser(String username) {
        return userRepo.getUser(username);
    }

    public void saveUser(User user) {

    }
}
