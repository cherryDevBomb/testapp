package service;

import model.User;

public interface IUserService {

    User findUser(String username);
    void saveUser(User user);
}
